package midletselector;

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
import com.sun.midp.lcdui.Resource;
import com.sun.midp.midlet.*;


public class AppliSelector extends MIDlet implements Runnable{
  private static AppliSelector instance;
  private Displayable1 displayable = new Displayable1(this);
  public String MIDletName;
  public Display display;

    public AppliSelector() {
    	instance = this;
        display = Display.getDisplay(this); // TBD: is this value value here?
    }

    public void run() {
        Scheduler scheduler = Scheduler.getScheduler();

        try {
            scheduler.register(MIDletState.createMIDlet(MIDletName));

            // Give the new MIDlet the screen by setting current to null
            display.setCurrent(null);

            // let another MIDlet be selected after MIDlet ends
            //displayable.MIDletLaunched=0; 
            return;
        } catch (Exception ex) {
            StringBuffer sb = new StringBuffer()
                .append(MIDletName)
/*
                .append(", ")
                .append(classname)
*/
                .append("\n")
                .append(Resource.getString("Exception"))
                .append(": ")
                .append(ex.toString());

            Alert a = new Alert(Resource.getString("Cannot start: "), 
                                sb.toString(), null, null);
            System.out.println("Unable to create MIDlet " + MIDletName);
            ex.printStackTrace();

            return;
        }
    }


  /** Main method */
  public void startApp() {
    Display.getDisplay(this).setCurrent(displayable);
  }

  /** Handle pausing the MIDlet */
  public void pauseApp() {
  }

  /** Handle destroying the MIDlet */
  public void destroyApp(boolean unconditional) {
  }

  /** Quit the MIDlet */
  public static void quitApp() {
/*
    instance.destroyApp(true);
    instance.notifyDestroyed();
    instance = null;
*/
  }

}
