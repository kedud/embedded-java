package midletselector;

public class Matrix {
	public int height;
	public int width;
	public int mat[];
	public Matrix(int pWidth, int pHeight) {
		height=pHeight;
		width=pWidth;
		mat=new int[pHeight*pWidth];
	}
}
