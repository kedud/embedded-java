// Name(s): PIERAGGI, DUDEK
// Image size for all measures : 100x100
// rotateMatrixV00
// Initial Time (simu) 		: > 20�000 000 ms
// Initial Time (plateform) : Non mesur�
//==========================================
// rotateMatrixV01
// Time (simu) 		: 171500, 227300, 228600 ms
// Time (plateform) : 50070, 59697, 59880 ms
// Optimisation description: Pre-compute cosinus and sinus values so that they are only computed once a frame
//==========================================
// rotateMatrixV02
// Time (simu) 		: 168800, 224600 ms
// Time (plateform) : 47645, 57269, 57465 ms
// Optimisation description: Avoid external functions calls (computeX and computeY)
//==========================================
// rotateMatrixV03
// Time (simu) 		: 111000, 141000 ms
// Time (plateform) : 33053, 38279, 38437 ms
// Optimisation description: Compute variables related to 'y' inside the first loop
//==========================================
// rotateMatrixV04
// Time (simu) 		: 16900, 17800 ms
// Time (plateform) : 6000, 6436, 6090, 6437 ms
// Optimisation description: Do not make float computation inside the loops
//==========================================
// rotateMatrixV05
// Time (simu) 		: 16209, 18434 ms
// Time (plateform) : 5589, 6031, 5677, 6022 ms
// Optimisation description: Remove out of bounds check
//==========================================
// rotateMatrixV06
// Time (simu) 		: 15418, 17717 ms
// Time (plateform) : 4823, 4643, 4644, 4824 ms
// Optimisation description: Use local variables instead class variables
//==========================================
// rotateMatrixV07
// Time (simu) 		: 15315, 14180 ms
// Time (plateform) : non mesur�
// Optimisation description: division done using bit shifting.



package midletselector;

import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;
import com.sun.midp.lcdui.Resource;
import com.sun.midp.midlet.*;
import java.lang.*;
import borneo.graphik.*;
import borneo.system.Borneo;
import borneo.system.BorneoFS;

public class Displayable1 extends Canvas implements CommandListener,Runnable {

	// image
	Matrix 	outputMatrix;
	
	// global variable used in the process example
	double 	globalPos;
	
	// signaling paint() is working
	int 	repainting;
	
	// processing time in (milliseconds ms)
	int		processTime, iteration;
	
	// DEBUG=1 : use of debug methods; DEBUG=0 : run on hardware platform
	final static int DEBUG=1;
	
	
	//**********************************
	// static class initialiser
	//**********************************
	static  {
		try	{
			
		} catch(Exception e) { e.printStackTrace(); }
	}

	//**********************************
	/** Constructor */
	//**********************************
	public Displayable1(AppliSelector pparent) {
		try	{
			setCommandListener(this);
		} catch(Exception e) { e.printStackTrace(); }

		// start a Thread (with interface (implements keyword))
		new Thread(this).start();
	}

	// *******************************
	// Thread starts here
	// *******************************
	public void run() {
		try {
			double 	dtmp;
			int 	desc, size,val;
			byte	fileBuf[];
		
			//==========================================================================================================================
			// Load MATRIX (image)
			// this code part shows how to load an PNG image from Borneo File System and how to convert it into a 1 dimensional matrix
			// "image file name from the root project".png directory space/Tab "name file name in the Borneo system".png
			// in this case we have :
			// images/java.png	images/java.png    	
			BImage imageMatrix=new BImage();
			// open file : file name must match with the name in BorneoU.list
			desc=BorneoFS.open("images/java.png");
			// set file read index at the end of the file (get the file size)
			size=(BorneoFS.seek(desc, 0, BorneoFS.SEEK_END));			
			Borneo.DebugPrint("fileSize="+size+"\n");
			// set file read index at the begining of the file
			BorneoFS.seek(desc, 0, BorneoFS.SEEK_SET);
			fileBuf=new byte[size];
			// read file and put data in fileBuf buffer
			BorneoFS.read(desc, fileBuf, 0, fileBuf.length);
			BorneoFS.close(desc);
			// decode data (PNG encoded) and put data in ARGB buffer
			val=imageMatrix.getARGB(fileBuf);
			if(val<=0) Borneo.DebugPrint("Unable to load file\n");
			else Borneo.DebugPrint("image size width="+imageMatrix.width+" height="+imageMatrix.height+" ARGBsize="+imageMatrix.ARGB.length+"\n");
			//==========================================================================================================================
			
			
			//==========================================================================================================================
			// Example of time evaluation
			// store current time (in milliseconds from the last system boot) in startTime (Time before sinus)
			long startTime=System.currentTimeMillis();
			dtmp=Math.sin(3.14/3, 10)*10000;
			// get the current time (Time after sinus)
			long stopTime=System.currentTimeMillis();
			
			// use constant DEBUG to activate/desactivate debug information
			if(DEBUG!=0) {
				// display time used to compute Math.sin : (time before sin()) - (time after sin())
				Borneo.DebugPrint("Sin Time="+(int)(System.currentTimeMillis()-startTime)+"\n");
				Borneo.DebugPrint("sin="+(int)(dtmp)+"\n");
			}
			dtmp=Math.cos(3.14/3, 10)*10000;
			if(DEBUG!=0) Borneo.DebugPrint("cos="+(int)(dtmp)+"\n");
			//==========================================================================================================================
			
			int oW = (int)(imageMatrix.width*1.5);
			int oH = (int)(imageMatrix.height*1.5);
			
			Borneo.DebugPrint("Output matrix size = "+oW+"x"+oH);
			outputMatrix=new Matrix(oW, oH); 
			Borneo.DebugPrint("Allocated");
			//==========================================================================================================================
			// Main loop
			while(true)	{
				// wait the end of the painting
				while(repainting==1) Thread.yield();
				iteration++;
				// Time before processing
				startTime=System.currentTimeMillis();
				
				//**********************************************
				// Rotation matrix is here
				processMatrix(imageMatrix, outputMatrix);
				//**********************************************
				
				// time after processing
				stopTime=System.currentTimeMillis();
				processTime=(int)(stopTime-startTime);
				// lock paint
				repainting=1;
				// launch paint
				repaint();
			}
			//==========================================================================================================================
		} catch(Exception e) { e.printStackTrace(); }
	}
	
	/***************************************************************************************
	 *                                     Rotate V00
	 *************************************************************************************** 
	 * Original rotation algorithm
	 * -> No optimization
	 * 
	 * Time (simu):     > 20�000 000 ms
	 * Time (platform):
	 * 
	 ***************************************************************************************/
	
	/**
	 * Compute rotated X coordinate
	 * @param x
	 * @param y
	 * @param angle
	 * @param acc
	 * @return rotated X coordinate
	 */
	int computeXv00(int x, int y, double angle, int acc) {
		return (int)(x*Math.cos(angle, acc) + y*Math.sin(angle, acc));
	}
	
	/**
	 * Compute rotated Y coordinate
	 * @param x
	 * @param y
	 * @param angle
	 * @param acc
	 * @return rotated Y coordinate
	 */
	int computeYv00(int x, int y, double angle, int acc) {
		return (int)((-1)*x*Math.sin(angle, acc) + y*Math.cos(angle, acc));
	}
	
	/**
	 * Original rotation algorithm
	 * 
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV00(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
				
		// Compute offsets
		xOffset = (-1)*(computeXv00((int)(inputMatrix.width/2.0), (int)(inputMatrix.height/2.0), globalPos, acc) - (int)(inputMatrix.width/2.0));
		yOffset = (-1)*(computeYv00((int)(inputMatrix.width/2.0), (int)(inputMatrix.height/2.0), globalPos, acc) - (int)(inputMatrix.height/2.0));
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				x1 = computeXv00(x, y, globalPos, acc);
				y1 = computeYv00(x, y, globalPos, acc);

				//Borneo.DebugPrint("x1 = "+x+"*"+(int)(Math.cos(globalPos, acc)*1000)+" + "+y+"*"+(int)(Math.sin(globalPos, acc)*1000)+"\n");
				//Borneo.DebugPrint("y1 = (-1)*"+x+"*"+(int)(Math.sin(globalPos, acc)*1000)+" + "+y+"*"+(int)(Math.cos(globalPos, acc)*1000)+"\n");
				//Borneo.DebugPrint("x1 = "+x1+" | y1 = "+y1+" | x = "+x+" | y = "+y+"\n");
				
				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Prevent out of bounds exceptions
				if( (xPostOffset > 0) && (yPostOffset > 0) && (xPostOffset < outputMatrix.height) && (yPostOffset < outputMatrix.width)) {
					// Apply new coordinates
					outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[y*inputMatrix.height + x];
				}
				else {
					Borneo.DebugPrint("Out of bounds : y1+offset = "+y1+"+"+yOffset+" ("+yPostOffset+") | x1+offset = "+x1+"+"+xOffset+" ("+xPostOffset+") | w = "+outputMatrix.width+" h = "+outputMatrix.height+"\n");
				}
				
			}
		}
		
	}
	
	/***************************************************************************************
	 *                                     Rotate V01
	 *************************************************************************************** 
	 * Optimization: Pre-compute cosinus and sinus values so that they are ony computed once a frame
	 * 
	 * Time (simu):      171500, 227300, 228600 ms
	 * Time (platform):  50070, 59697, 59880 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Compute rotated X coordinate
	 * cos and sin values are directly passed 
	 * @param x
	 * @param y
	 * @param cos
	 * @param sin
	 * @return rotated X coordinate
	 */
	int computeXv01(int x, int y, double cos, double sin) {
		return (int)(x*cos + y*sin);
	}
	
	/**
	 * Compute rotated Y coordinate
	 * cos and sin values are directly passed
	 * @param x
	 * @param y
	 * @param cos
	 * @param sin
	 * @return rotated Y coordinate
	 */
	int computeYv01(int x, int y, double cos, double sin) {
		return (int)((-1)*x*sin + y*cos);
	}
	
	/**
	 * Pre-compute cosinus and sinus values so that they are ony computed once a frame
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV01(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		
		// Pre-compute cos and sin values
		cos = Math.cos(globalPos, acc);
		sin = Math.sin(globalPos, acc);
		
		// Compute offsets
		xOffset = (-1)*(computeXv01((int)(inputMatrix.width/2.0), (int)(inputMatrix.height/2.0), cos, sin) - (int)(inputMatrix.width/2.0));
		yOffset = (-1)*(computeYv01((int)(inputMatrix.width/2.0), (int)(inputMatrix.height/2.0), cos, sin) - (int)(inputMatrix.height/2.0));
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				x1 = computeXv01(x, y, cos, sin);
				y1 = computeYv01(x, y, cos, sin);

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Prevent out of bounds exceptions
				if( (xPostOffset > 0) && (yPostOffset > 0) && (xPostOffset < outputMatrix.height) && (yPostOffset < outputMatrix.width)) {
					// Apply new coordinates
					outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[y*inputMatrix.height + x];
				}
				else {
					Borneo.DebugPrint("Out of bounds : y1+offset = "+y1+"+"+yOffset+" ("+yPostOffset+") | x1+offset = "+x1+"+"+xOffset+" ("+xPostOffset+") | w = "+outputMatrix.width+" h = "+outputMatrix.height+"\n");
				}
				
			}
		}
		
	}
	
	
	/***************************************************************************************
	 *                                     Rotate V02
	 *************************************************************************************** 
	 * Optimization: Avoid external functions calls (computeX and computeY)
	 * 
	 * Time (simu):      168800, 224600 ms
	 * Time (platform):  47645, 57269, 57465 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV02(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		
		// Pre-compute cos and sin values
		cos = Math.cos(globalPos, acc);
		sin = Math.sin(globalPos, acc);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputMatrix.width*cos/2.0 + inputMatrix.height*sin/2.0 - inputMatrix.width/2.0);
		yOffset = (-1)*(int)(inputMatrix.height*cos/2.0 - inputMatrix.width*sin/2.0 - inputMatrix.height/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				x1 = (int)(x*cos + y*sin);
				y1 = (int)((-1)*x*sin + y*cos);

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Prevent out of bounds exceptions
				if( (xPostOffset > 0) && (yPostOffset > 0) && (xPostOffset < outputMatrix.height) && (yPostOffset < outputMatrix.width)) {
					// Apply new coordinates
					outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[y*inputMatrix.height + x];
				}
				else {
					Borneo.DebugPrint("Out of bounds : y1+offset = "+y1+"+"+yOffset+" ("+yPostOffset+") | x1+offset = "+x1+"+"+xOffset+" ("+xPostOffset+") | w = "+outputMatrix.width+" h = "+outputMatrix.height+"\n");
				}
				
			}
			
		}
		
	}
	
	
	/***************************************************************************************
	 *                                     Rotate V03
	 *************************************************************************************** 
	 * Optimization: Compute variables related to 'y' inside the first loop
	 * 
	 * Time (simu):     111000, 141000 ms
	 * Time (platform): 33053, 38279, 38437 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV03(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		double ysin, ycos;
		int yH;
		
		// Pre-compute cos and sin values
		cos = Math.cos(globalPos, acc);
		sin = Math.sin(globalPos, acc);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputMatrix.width*cos/2.0 + inputMatrix.height*sin/2.0 - inputMatrix.width/2.0);
		yOffset = (-1)*(int)(inputMatrix.height*cos/2.0 - inputMatrix.width*sin/2.0 - inputMatrix.height/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			// Compute all variables related to y here
			ysin = y*sin;
			ycos = y*cos;
			yH = y*inputMatrix.height;
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				x1 = (int)(x*cos + ysin);
				y1 = (int)((-1)*x*sin + ycos);

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Prevent out of bounds exceptions
				if( (xPostOffset > 0) && (yPostOffset > 0) && (xPostOffset < outputMatrix.height) && (yPostOffset < outputMatrix.width)) {
					// Apply new coordinates
					outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[yH + x];
				}
				else {
					Borneo.DebugPrint("Out of bounds : y1+offset = "+y1+"+"+yOffset+" ("+yPostOffset+") | x1+offset = "+x1+"+"+xOffset+" ("+xPostOffset+") | w = "+outputMatrix.width+" h = "+outputMatrix.height+"\n");
				}
				
			}
			
		}
		
	}
	
	
	/***************************************************************************************
	 *                                     Rotate V04
	 *************************************************************************************** 
	 * Optimization: Do not make float computation inside the loops
	 * 
	 * Time (simu):      16900, 17800 ms
	 * Time (platform):  6000, 6436, 6090, 6437 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV04(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		int cosInt, sinInt;
		int ysin, ycos;
		int yH;
		final int multiplier = 1024;
		
		// Pre-compute cos and sin values
		cos = Math.cos(globalPos, acc);
		sin = Math.sin(globalPos, acc);
		
		// Compute cos and sin as Integers
		cosInt = (int)(cos*multiplier);
		sinInt = (int)(sin*multiplier);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputMatrix.width*cos/2.0 + inputMatrix.height*sin/2.0 - inputMatrix.width/2.0);
		yOffset = (-1)*(int)(inputMatrix.height*cos/2.0 - inputMatrix.width*sin/2.0 - inputMatrix.height/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			// Compute all variables related to y here
			// Compute unsing integer values of cos and sin
			ysin = y*sinInt/multiplier;
			ycos = y*cosInt/multiplier;
			yH = y*inputMatrix.height;
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				// Compute unsing integer values of cos and sin
				x1 = x*cosInt/multiplier + ysin;
				y1 = (-1)*x*sinInt/multiplier + ycos;

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Prevent out of bounds exceptions
				if( (xPostOffset > 0) && (yPostOffset > 0) && (xPostOffset < outputMatrix.height) && (yPostOffset < outputMatrix.width)) {
					// Apply new coordinates
					outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[yH + x];
				}
				else {
					Borneo.DebugPrint("Out of bounds : y1+offset = "+y1+"+"+yOffset+" ("+yPostOffset+") | x1+offset = "+x1+"+"+xOffset+" ("+xPostOffset+") | w = "+outputMatrix.width+" h = "+outputMatrix.height+"\n");
				}
				
			}
			
		}
		
	}
	
	/***************************************************************************************
	 *                                     Rotate V05
	 *************************************************************************************** 
	 * Optimization: Remove out of bounds check
	 * 
	 * Time (simu):     16209, 18434 ms
	 * Time (platform): 5589, 6031, 5677, 6022 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV05(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		int cosInt, sinInt;
		int ysin, ycos;
		int yH;
		final int multiplier = 1024;
		
		// Pre-compute cos and sin values
		cos = Math.cos(globalPos, acc);
		sin = Math.sin(globalPos, acc);
		
		// Compute cos and sin as Integers
		cosInt = (int)(cos*multiplier);
		sinInt = (int)(sin*multiplier);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputMatrix.width*cos/2.0 + inputMatrix.height*sin/2.0 - inputMatrix.width/2.0);
		yOffset = (-1)*(int)(inputMatrix.height*cos/2.0 - inputMatrix.width*sin/2.0 - inputMatrix.height/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(globalPos*1000)+")\n");
		for(y=0; y< inputMatrix.height; y++) {
			
			// Compute all variables related to y here
			// Compute unsing integer values of cos and sin
			ysin = y*sinInt/multiplier;
			ycos = y*cosInt/multiplier;
			yH = y*inputMatrix.height;
			
			for(x=0; x<inputMatrix.width; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				// Compute unsing integer values of cos and sin
				x1 = x*cosInt/multiplier + ysin;
				y1 = (-1)*x*sinInt/multiplier + ycos;

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputMatrix.height - inputMatrix.height)/2)+yOffset;
				xPostOffset = x1+((outputMatrix.width - inputMatrix.width)/2)+xOffset;
				
				// Apply new coordinates
				outputMatrix.mat[yPostOffset*outputMatrix.width + xPostOffset] = inputMatrix.ARGB[yH + x];
			}
			
		}
		
	}
	
	/***************************************************************************************
	 *                                     Rotate V06
	 *************************************************************************************** 
	 * Optimization: Use local variables instead class variables
	 * 
	 * Time (simu):     15418, 17717 ms
	 * Time (platform): 4823, 4643, 4644, 4824 ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV06(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		int cosInt, sinInt;
		int ysin, ycos;
		int yH;
		final int multiplier = 1024;
		
		// Use local variables instead class variables
		double angle = globalPos;
		int inputWidth = inputMatrix.width;
		int inputHeight = inputMatrix.height;
		int[] inMatrix = inputMatrix.ARGB;
		int outputWidth = outputMatrix.width;
		int outputHeight = outputMatrix.height;
		int[] outMatrix = outputMatrix.mat;
		
		// Pre-compute cos and sin values
		cos = Math.cos(angle, acc);
		sin = Math.sin(angle, acc);
		
		// Compute cos and sin as Integers
		cosInt = (int)(cos*multiplier);
		sinInt = (int)(sin*multiplier);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputWidth*cos/2.0 + inputHeight*sin/2.0 - inputWidth/2.0);
		yOffset = (-1)*(int)(inputHeight*cos/2.0 - inputWidth*sin/2.0 - inputHeight/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(angle*1000)+")\n");
		for(y=0; y< inputHeight; y++) {
			
			// Compute all variables related to y here
			// Compute using integer values of cos and sin
			ysin = y*sinInt/multiplier;
			ycos = y*cosInt/multiplier;
			yH = y*inputHeight;
			
			for(x=0; x<inputWidth; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				// Compute unsing integer values of cos and sin
				x1 = x*cosInt/multiplier + ysin;
				y1 = (-1)*x*sinInt/multiplier + ycos;

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputHeight - inputHeight)/2)+yOffset;
				xPostOffset = x1+((outputWidth - inputWidth)/2)+xOffset;
				
				// Apply new coordinates
				outMatrix[yPostOffset*outputWidth + xPostOffset] = inMatrix[yH + x];
			}
			
		}
		
	}
	
	
	
	/***************************************************************************************
	 *                                     Rotate V07
	 *************************************************************************************** 
	 * Optimization: division done using bit shifting.
	 * 
	 * Time (simu):     15315, 14180 ms
	 * Time (platform): ms
	 * 
	 ***************************************************************************************/
	
	/**
	 * Rotation algorithm
	 * @param inputMatrix
	 * @param outputMatrix
	 */
	void rotateMatrixV07(BImage inputMatrix, Matrix outputMatrix)
	{
		// Local resources
		int y, x;
		int x1,y1;
		final int acc = 10;
		int xOffset = 0, yOffset = 0;
		int xPostOffset, yPostOffset;
		double cos, sin;
		int cosInt, sinInt;
		int ysin, ycos;
		int yH;
		final int multiplier = 1024;
		final int divider = 10;

		
		// Use local variables instead class variables
		double angle = globalPos;
		int inputWidth = inputMatrix.width;
		int inputHeight = inputMatrix.height;
		int[] inMatrix = inputMatrix.ARGB;
		int outputWidth = outputMatrix.width;
		int outputHeight = outputMatrix.height;
		int[] outMatrix = outputMatrix.mat;
		
		// Pre-compute cos and sin values
		cos = Math.cos(angle, acc);
		sin = Math.sin(angle, acc);
		
		// Compute cos and sin as Integers
		cosInt = (int)(cos*multiplier);
		sinInt = (int)(sin*multiplier);
		
		// Compute offsets
		xOffset = (-1)*(int)(inputWidth*cos/2.0 + inputHeight*sin/2.0 - inputWidth/2.0);
		yOffset = (-1)*(int)(inputHeight*cos/2.0 - inputWidth*sin/2.0 - inputHeight/2.0);
		
		// Rotate
		Borneo.DebugPrint("Before (angle = "+(int)(angle*1000)+")\n");
		for(y=0; y< inputHeight; y++) {
			
			// Compute all variables related to y here
			// Compute using integer values of cos and sin
			ysin = y*sinInt;
			ycos = y*cosInt;
			yH = y*inputHeight;
			
			for(x=0; x<inputWidth; x++) {
					
				// Compute x1 and y1, the rotated coordinates of the pixel
				// Compute unsing integer values of cos and sin
				x1 = (x*cosInt + ysin) >> divider;
				y1 = ((-1)*x*sinInt + ycos) >> divider;

				// Compute the coordinates with required offsets
				yPostOffset = y1+((outputHeight - inputHeight)/2)+yOffset;
				xPostOffset = x1+((outputWidth - inputWidth)/2)+xOffset;
				
				// Apply new coordinates
				outMatrix[yPostOffset*outputWidth + xPostOffset] = inMatrix[yH + x];
			}
			
		}
		
	}
	
	//********************************************************
	// method : perform a specific operation on the Matrix
	// fill this method with the matrix rotation algorithm
	//********************************************************
	void processMatrix(BImage inputMatrix, Matrix outputMatrix) {
		
		/**
		Matrix rotation:
		initial image -> rotated image
		X'=  X*cos(angle) + Y*sin(angle)
		Y'= -X*sin(angle) + Y*cos(angle)
		rotated image -> initial image
		X= X'*cos(angle) - Y'*sin(angle)
		Y= X'*sin(angle) - Y'*cos(angle)
		 */
				
		int i;
		final double pi = 3.1415926536;
		
		for(i=0; i< outputMatrix.width*outputMatrix.height; i++) {
			outputMatrix.mat[i] = 0;
		}
		
		rotateMatrixV07(inputMatrix, outputMatrix);
		
		Borneo.DebugPrint("After\n");
		globalPos+=(pi/4.0);
		if(globalPos >= 2*pi)
			globalPos = 0;
	}
	
	//******************************************
	// Draw items on screen
	//******************************************
	protected void paint(Graphics g) {
		g.setColor(0xffffff);
		g.fillRect(0,0,480,272);
		g.drawRGB(outputMatrix.mat, 0, outputMatrix.width, 50, 50, outputMatrix.width, outputMatrix.height, false);
		g.setColor(0);
		g.drawString("process time="+processTime+" ms ite="+iteration, 10, 200, 0);
		repainting=0;
	}

	public void commandAction(Command command, Displayable displayable) {
	}

	protected void showNotify() {
	}

	protected void hideNotify() {
	}

	// method launched when the touch pad is pressed
	public void pointerPressed(int x, int y)  {
	}

	// method launched when the touch pad is dragged (the pointer stay pressed andslides on the touchpad)
	public void pointerDragged(int x, int y)  {
	}

	// method launched when the touch pad is released
	public void pointerReleased(int x, int y)  {
	}

	// method launched when a key is released
	public void keyReleased(int keyCode)  {
	}

	// method launched when a key is pressed
	public void keyPressed(int keyCode) {
	}
	
}

