package midletselector;

public class Math {
	public static double exp(double val, int exp) {
		double 	res=1.f;
		int		i;
		
		for(i=0; i<exp; i++) {
			res=val*res;
			//Borneo.DebugPrint("res["+i+"]="+(int)(res*10000)+"\n");
		}
		return res;
	}

	public static double fact(int n) {
		int i;
		double res=1.f;
		
		for(i=2; i<=n; i++) res*=i;
		
		return res;
	}
	
	public static double sin(double angle, int acc) {
		double sin=angle;
		int i;
		for(i=1; i<acc; i++) {
			//Borneo.DebugPrint("exp["+i+"]="+(int)(exp(angle, (2*i)+1)*10000)+"\n");
			//Borneo.DebugPrint("fact["+i+"]="+(int)(fact((2*i)+1))+"\n");
			//Borneo.DebugPrint("exp/fact="+(int)((exp(angle, (2*i)+1)/fact((2*i)+1))*10000)+"\n");
			if((i&1)!=0) sin-=exp(angle, (2*i)+1)/fact((2*i)+1);
			else sin+=exp(angle, (2*i)+1)/fact((2*i)+1);
			//Borneo.DebugPrint("sin["+i+"]="+(int)(sin*10000)+"\n");

		}
		return sin;
	}

	public static double cos(double angle, int acc) {
		double cos=1;
		int i;
		for(i=1; i<acc; i++) {
			if((i&1)==1) cos-=exp(angle, 2*i)/fact(2*i);
			else cos+=exp(angle, 2*i)/fact(2*i);
		}
		return cos;
	}
}
